class CreateUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :urls do |t|
      t.string :full_url
      t.string :short_url, index: true
    end
  end
end
