Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :urls, param: :short_url,
                   only: %i[index create show],
                   constraints: { :short_url => /[^\/]+/ }
  root 'urls#index'
  match '/:short_url', to: 'urls#show', via: :get
  match '/', to: 'urls#create', via: :post
end
