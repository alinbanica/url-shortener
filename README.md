# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

Ruby version

  * 2.3.3

Rails version

  * 5.2.1

Database info

  * It's using SQLite database

Database initialization

  * rake db:migrate

How to run the test suite

  * bundle exec rspec spec

How to test from console

  * Create a short url for a posted URL

    - curl localhost:3000/ -XPOST -d '{ "url": "http://www.farmdrop.com" }' -H "Content-Type: application/json"

        - {"short_url":"/bcbb5f","url":"http://www.farmdrop.com"}

  * Redirection to the previous posted URL

    - curl -v localhost:3000/bcbb5f

Assumptions:

* 'Without using an external database' => means not using already existing services or not using Postgres or Redis databases
* I did not used docker so I did not create a dockerfile
* I am not a frontend developer so I did not add any frontend