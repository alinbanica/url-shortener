require 'rails_helper'

RSpec.describe 'UriService' do
  let(:url) { '' }
  let(:uri_service) { UriService.new(url) }

  describe '#full_url' do
    context 'when input url is empty' do
      it 'expects to return nil full_url' do
        expect(uri_service.full_url).to be_empty
      end
    end

    context 'when input url is invalid url' do
      let(:url) { 'test|cucubau?test=1' }
      it 'expects to return nil full_url' do
        expect(uri_service.full_url).to be_empty
      end
    end

    context 'when input url is valid url' do
      context 'when having http://' do
        let(:url) { 'http://cucubau.com?test=1' }
        it 'expects to return full_url as input url' do
          expect(uri_service.full_url).to eq(url)
        end
      end

      context 'not having http://' do
        let(:url) { 'cucubau.com?test=1' }
        it 'expects to return input url with http appended' do
          expect(uri_service.full_url).to eq("http://#{url}")
        end
      end
    end
  end

  describe '#short_url' do
    let(:url) { 'http://www.farmdrop.com' }
    context 'when short_url do not exists' do
      it 'should generate a 6 digit short url' do
        expect(uri_service.short_url.length).to eq(6)
      end
    end

    context 'when short_url exists' do
      let(:existing_short_url) { ShortUrlService.new(url).generate_url }

      it 'should generate a 7 digit short url' do
        create(:url, full_url: 'http://farmado.com', short_url: existing_short_url)

        new_short_url = uri_service.short_url
        expect(new_short_url.length).to eq(7)
        expect(new_short_url).to include(existing_short_url)
      end
    end
  end
end
