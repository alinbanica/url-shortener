require 'rails_helper'

RSpec.describe 'UriService' do
  let(:url) { 'http://www.farmdrop.com' }
  let(:step) { 0 }
  let(:shor_url_service) { ShortUrlService.new(url, step: step) }

  describe '#generate_url' do
    it 'should generate a 6 digit short url' do
      expect(shor_url_service.generate_url.length).to eq(6)
    end

    context 'when step is send as param' do
      let(:step) { 3 }
      it 'should generate a 9 digit short url' do
        expect(shor_url_service.generate_url.length).to eq(9)
      end
    end
  end
end
