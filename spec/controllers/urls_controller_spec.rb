require 'rails_helper'

RSpec.shared_examples 'not creating a new url' do
  it 'should not create a new url' do
    expect { subject }.to change { Url.count }.by(0)
  end
end

RSpec.shared_examples 'creating a new url' do
  it 'should create a new url' do
    expect { subject }.to change { Url.count }.by(1)
  end
end

RSpec.shared_examples 'returning an error' do
  it 'should return error response' do
    subject
    response_body = JSON.parse(response.body)
    expect(response_body['status']).to eq('error')
  end
end

RSpec.shared_examples 'returning success' do
  it 'should return success response' do
    subject
    response_body = JSON.parse(response.body)
    expect(response_body['url']).to eq('http://www.farmdrop.com')
    expect(response.body['short_url']).to be_present
  end
end

RSpec.describe UrlsController, type: :controller do
  describe '#index' do
    subject { get :index }
    it 'returns a success response' do
      subject
      expect(response.status).to eq(200)
    end

    context 'when there are no urls in database' do
      it 'returns empty list' do
        subject
        response_body = JSON.parse(response.body)
        expect(response_body).to be_empty
      end
    end

    context 'when there are urls in database' do
      let!(:first_url) { create(:url, short_url: 'abc123') }
      let!(:second_url) { create(:url, short_url: 'def896') }

      it 'returns the existing urls' do
        subject
        short_urls = JSON.parse(response.body)
                         .map { |url| url['short_url'] }
                         .compact
        expected_short_urls = ['/abc123', '/def896']
        expect(short_urls).to match_array(expected_short_urls)
      end
    end
  end

  describe '#create' do
    let(:new_url) { '' }
    let(:params) { { url: new_url } }

    subject { post :create, params: params }

    context 'when url not present' do
      it_behaves_like 'not creating a new url'
      it_behaves_like 'returning an error'
    end

    context 'when url is invalid' do
      let(:new_url) { 'test|cucuvau?test=123' }
      it_behaves_like 'not creating a new url'
      it_behaves_like 'returning an error'
    end

    context 'when url already exists' do
      let(:new_url) { 'http://www.farmdrop.com' }

      before :each do
        create(:url, full_url: new_url,
                     short_url: ShortUrlService.new(new_url).generate_url)
      end

      it_behaves_like 'not creating a new url'
      it_behaves_like 'returning success'
    end

    context 'when url is valid' do
      let(:new_url) { 'http://www.farmdrop.com' }

      it_behaves_like 'creating a new url'
      it_behaves_like 'returning success'
    end

    context 'when url is valid but http is missing' do
      let(:new_url) { 'www.farmdrop.com' }

      it_behaves_like 'creating a new url'
      it_behaves_like 'returning success'

      it 'should add http:// in front of the url' do
        subject
        response_body = JSON.parse(response.body)
        expect(response_body['url']).to eq('http://www.farmdrop.com')
      end
    end

    context 'when short_url already exists' do
      let(:new_url) { 'http://www.farmdrop.com' }
      let(:short_url) { ShortUrlService.new(new_url).generate_url }

      before :each do
        create(:url, full_url: 'http://farmado.com', short_url: short_url)
      end

      it_behaves_like 'creating a new url'
      it_behaves_like 'returning success'

      it 'returns a different and longer short_url' do
        subject
        response_body = JSON.parse(response.body)
        expect(response_body['short_url']).to_not eq(short_url)
        expect(response_body['short_url'].length).to be > short_url.length
      end
    end
  end

  describe '#show' do
    let(:short_url) { 'abc123' }
    let(:params) { { short_url: short_url } }
    subject { get :show, params: params }

    context 'when short url exists' do
      let(:full_url) { 'http://www.farmdrop.com' }

      it 'expects to redirect to the full url' do
        create(:url, full_url: full_url, short_url: short_url)
        expect(subject).to redirect_to(full_url)
      end
    end

    context 'when short url not exists' do
      it 'expects to redirect to the full url' do
        expect(subject).to render_template(:show)
      end
    end
  end
end
