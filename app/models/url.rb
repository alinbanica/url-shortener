class Url < ApplicationRecord
  validates :short_url, presence: true, uniqueness: true
  validates :full_url, url: true, uniqueness: { scope: :short_url,
                                                message: 'should have a unique short_url' }
end
