class UriService
  def initialize(url)
    @url = url
  end

  def valid_url?
    URI.parse(format_url)&.host.present? rescue false
  end

  def full_url
    return '' unless valid_url?
    format_url.to_s
  end

  def short_url
    i = 0
    generated_url = genearte_short_url(i)

    while short_url_exists?(generated_url)
      i += 1
      generated_url = genearte_short_url(i)
    end

    generated_url
  end

  private

  attr_reader :url

  def format_url
    return url if !!url.match(/^https?/)
    "http://#{url}"
  end

  def short_url_exists?(short_generated_url)
    Url.where(short_url: short_generated_url)
       .where.not(full_url: full_url)
       .exists?
  end

  def genearte_short_url(step = 0)
    ShortUrlService.new(format_url, step: step).generate_url
  end
end
