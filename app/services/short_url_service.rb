class ShortUrlService
  SHORT_URL_LENGTH = 6

  def initialize(url, step: 0)
    @url = url
    @step = step
  end

  def generate_url
    Digest::MD5.hexdigest(url)[0...(SHORT_URL_LENGTH + step)]
  end

  private

  attr_reader :url, :step
end
