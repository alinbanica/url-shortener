class UrlSerializer < ActiveModel::Serializer
  attribute :short_url
  attribute :full_url, key: :url

  def short_url
    "/#{object.short_url}"
  end
end
