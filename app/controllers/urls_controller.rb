class UrlsController < ApplicationController
  before_action :url, only: :show

  def index
    render json: Url.all
  end

  def create
    if uri_service.valid_url?
      render json: Url.find_or_create_by(create_params)
    else
      render json: { status: 'error', message: 'Invalid URL' }
    end
  end

  def show
    redirect_to url.full_url if url
  end

  private

  def url_params
    params.permit(:url, :short_url)
  end

  def uri_service
    @uri_service ||= UriService.new(url_params[:url])
  end

  def create_params
    {
      full_url: uri_service.full_url,
      short_url: uri_service.short_url
    }
  end

  def url
    @url ||= Url.find_by(url_params.slice(:short_url))
  end
end
